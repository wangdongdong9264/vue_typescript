# vue_Typescript

## 环境

node: 10.16.3

### 待解决

* 功能项

  1. 动态路由 `目前版本的vue 不够完善` 残废(二级路由不行)
  2. 权限路由
  3. vuex
  5. axios 请求封装
  6. eslint
  7. git变量-不同分支 待定
  8. 登录相关

* 优化项

  1. js多线程优化 happypack
  2. vue-router 懒加载
  3. 添加进度条
  4. antd 按需加载 `babel-plugin-import`
