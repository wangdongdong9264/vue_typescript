import Vue from 'vue'
import Router from 'vue-router'
import Layout from './layout/layout.vue'

import edit from './view/edit/edit.vue'
import selfEvaluation from './view/selfEvaluation/selfEvaluation.vue'
import architecture from './view/architecture/architecture.vue'
import verify from './view/verify/verify.vue'
import home from './view/home/home.vue'
import login from './view/login/login.vue'

Vue.use(Router)

export const baseRouter = [
  {
    path: '/okr',
    name: 'okr',
    component: Layout,
    meta: {
      title: '我的OKR',
      icon: ''
    },
    children: [
      {
        path: 'new',
        name: 'new',
        component: edit,
        meta: {
          title: 'okr',
          icon: ''
        },
      },
      {
        path: 'self',
        name: 'self',
        component: selfEvaluation,
        meta: {
          title: '自评',
          icon: ''
        },
      }
    ]
  },
  {
    path: '/all',
    name: 'all',
    component: Layout,
    meta: {
      title: '全部OKR',
      icon: ''
    },
    children: [
      {
        path: 'chart',
        name: 'chart',
        component: architecture,
        meta: {
          title: '架构图',
          icon: ''
        },
      }
    ]
  },
  {
    path: '/verify',
    name: 'verify',
    component: Layout,
    meta: {
      title: '审核OKR',
      icon: ''
    },
    children: [
      {
        path: 'verifyOKR',
        name: 'verifyOKR',
        component: verify,
        meta: {
          title: '审核',
          icon: ''
        },
      }
    ]
  }
]
export const hideRouter = [
  {
    path: '/',
    component: Layout,
    name: '',
    meta: {
      title: '首页',
      icon: ''
    },
    children: [
      {
        path: '/',
        name: 'view',
        component: home,
        meta: {
          title: '首页',
          icon: ''
        }
      }
    ]
  },
  {
    path: '/login',
    name: 'login',
    meta: {
      title: '登录',
      icon: ''
    },
    component: login
  }
]

export default new Router({
  mode: 'history',
  routes: [...baseRouter, ...hideRouter]
})