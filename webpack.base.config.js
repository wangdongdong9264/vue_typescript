const path = require("path");
const VueLoaderPlugin = require("vue-loader/lib/plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HappyPack = require("happypack");
const os = require("os");
const happyThreadPool = HappyPack.ThreadPool({ size: os.cpus().length });

module.exports = {
  entry: "./src/index.ts",
  output: {
    path: path.resolve(__dirname, "dist"),
    publicPath: "/",
    filename: "index.[hash:8].js"
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".vue", ".json"],
    alias: {
      vue$: "vue/dist/vue.esm.js"
      // "@": path.join(__dirname, "src")
    }
  },
  optimization: {
    splitChunks: {
      chunks: "all"
    }
  },
  externals: {},
  module: {
    rules: [
      {
        test: /\.vue$/,
        exclude: /node_modules/,
        loader: "vue-loader",
        options: {
          loaders: {
            scss: "vue-style-loader!css-loader!sass-loader",
            sass: "vue-style-loader!css-loader!sass-loader?indentedSyntax"
          }
        }
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        exclude: /node_modules/,
        loader: "url-loader",
        options: {
          limit: 8192,
          name: "[name].[ext]?[hash]",
          outputPath: "/images/"
        }
      },
      {
        test: /\.css$/,
        use: [
          "style-loader",
          {
            loader: MiniCssExtractPlugin.loader
          },
          "css-loader"
        ]
      },
      {
        test: /\.scss$/,
        use: [
          "style-loader",
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              // you can specify a publicPath here
              // by default it uses publicPath in webpackOptions.output
            }
          },
          "css-loader",
          {
            loader: "postcss-loader"
          },
          "sass-loader"
        ]
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: "happypack/loader?id=happyBabel"
          }
        ]
      },
      {
        test: /\.ts(x?)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: "babel-loader"
            // loader: "happypack/loader?id=happyBabel"
          },
          {
            loader: "ts-loader",
            // loader: "happypack/loader?id=happyTS",
            options: {
              appendTsSuffixTo: [/\.vue$/]
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new VueLoaderPlugin(),
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // all options are optional
      filename: "static/[name].[hash:8].css",
      ignoreOrder: false // Enable to remove warnings about conflicting order
    }),
    new HappyPack({
      id: "happyBabel",
      loaders: [
        {
          loader: "babel-loader"
        }
      ],
      threadPool: happyThreadPool,
      //允许 HappyPack 输出日志
      verbose: true
    })
    // new HappyPack({
    //   id: "happyTS",
    //   loaders: [
    //     {
    //       loader: 'babel-loader'
    //       // loader: "happypack/loader?id=happyBabel"
    //     },
    //     {
    //       loader: "ts-loader",
    //       // loader: "happypack/loader?id=happyTS",
    //       options: {
    //         appendTsSuffixTo: [/\.vue$/]
    //       }
    //     }
    //   ],
    //   threadPool: happyThreadPool,
    //   verbose: true
    // })
  ]
};
